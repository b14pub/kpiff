<?php

namespace Drupal\kpiff\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Kpiff controller.
 */
class KpiffController extends ControllerBase {

  /**
   * Stats.
   */
  public function stats() {
    return ['#theme' => "kpiff_stats"];
  }

}
