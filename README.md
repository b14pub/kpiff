# Kpiff Proof of Concept



## Getting started in Drupal websites

This is an early proof of concept for the KPIFF framework

The repo contains a Drupal module which can be installed into any existing Drupal application.

KPIFF does a count of existing drupal content (nodes) grouped by content type

After installation go to your-website.com/admin/kpiff/stats to see statistics of raw content count and "Brugerrejse" tagging.

## Usage in non-Drupal websites

The js/kpiff.js can be included as a stand alone script into websites not using Drupal.

## Dependencies

The module relies on the Siteimprove _sz javascript object to track events.
