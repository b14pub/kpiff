var kpiff = this.kpiff || {};

var kpiff_base = function(ns) {
  "use strict";

  const version = "1.0.4";

  ns.journey_type = {
    "info" : "Brugerrejse Info",
    "tilbud" : "Brugerrejse Tilbud",
    "sag" : "Brugerrejse Sagsbehandling",
    "navigation" : "Brugerrejse Navigation",
    "andet" : "Brugerrejse Andet",
    "ingen" : "Brugerrejse Ingen",
  }

  ns.trackers = [
    {
      "handler" : "handle_info_click",
      "on" : "click",
      "selectors" : [
        "div.kk-accordion",
        "button.more-info-btn",
      ]
    },

    {
      "handler" : "handle_external_link",
      "on" : "click",
      "selectors" : [
        '.paragraph--type--cta-box a[href*="://"]',
        '.paragraph--type--text a[href*="://"]',
        '.paragraph--type--accordion a[href*="://"]',
        '.paragraph--type--factbox a[href*="://"]',
        '.paragraph--type--contact-box .kk-accordion a[href*="://"]',
        '.field--name-body a[href*="://"]',
      ],
    },

    // Event tilbud journeys
    {
      "handler" : "handle_external_link",
      "on" : "click",
      "selectors" : [
        '.paragraph--type--price a[href*="://"]',
        '.event-info a[href*="://"]',
      ],
      "force_journey" : ns.journey_type.tilbud,
    },

    {
      "handler" : "handle_file_link",
      "on" : "click",
      "selectors" : [
        'a[href*=".pdf"]',
      ],
    },


    {
      "handler" : "handle_maps",
      "on" : "click",
      "selectors" : [
        "button.c-go-back"
      ],
    },

    {
      "handler" : "handle_default_contact",
      "on" : "click",
      "selectors" : [
        ".paragraph--type--contact-box"
      ]
    },

    {
      "handler" : "handle_info_linger",
      "type" : "custom",
    },

    {
      "handler" : "handle_frontdesk_init",
      "type" : "custom",
      "selectors" : [
        "body.mdc-typography .section-buttons a",
      ],
    },


    {
      "handler" : "handle_frontdesk_click",
      "on" : "click",
      "selectors" : [
        "body.mdc-typography .section-buttons a",
      ],
    },

  ];

  // Kpiff Settings
  ns.obs_visibilityThreshold = 0.8; // Minimum percent visible of tracked object
  ns.obs_yOffsetCutoff = 200; // Minimum pixels scrolled to kick off scrolled engagement of object
  ns.obs_timeCutoff = 5000; // Minimum mikroseconds passed before intersection counts
  ns.obs_timeRequired = 5000; // Minimum mikroseconds of valid intersection
  ns.lingerTimeRequired = 15000; // Mikroseconds before perceiving engagement

  ns.init = function() {
    console.log("KPIFF version: " + version + " Alfa");
  }

  ns.checkVisibility = function(hits) {

    // Discard low scrolling
    if (window.pageYOffset < ns.obs_yOffsetCutoff) { return; }

    hits.forEach(function(hit) {
      // Discard high speed events
      var is_valid_hit = hit.time > ns.obs_timeCutoff && hit.isIntersecting && hit.intersectionRatio > ns.obs_visibilityThreshold;
      if (is_valid_hit) {

        // One timer per element
        if (hit.target.intersectionTimer || hit.target.eventSent) {

        } else {
          hit.target.intersectionTimer = setTimeout( function() {
            b14site.analytics.sendEvent(hit.target.hitCategory, hit.target.hitAction, hit.target.hitLabel, hit.target);
          }, ns.obs_timeRequired);
          hit.target.intersectionTimer = null;
          hit.target.eventSent = true;
        }


      } else {
        // Cancel too quick intersections
        if (hit.target.intersectionTimer) {
          clearTimeout(hit.target.intersectionTimer);
          hit.target.intersectionTimer = null;
        }
      }

    })

  }

  // Create an observer for kpiff
  ns.observer = new IntersectionObserver(ns.checkVisibility, {
    threshold: ns.obs_visibilityThreshold
  });


  ns.observe = function(context, selector, category, action, label) {
    var element = context.querySelector(selector);

    if (element) {
      element.hitCategory = category;
      element.hitAction = action;
      element.hitLabel = label;
      ns.observer.observe( element );
    }
  }


  ns.add_listeners = function(context, settings) {
    console.log("KPIFF add listeners");

    ns.trackers.forEach(function(tracker) {

      var tracker_type = tracker.type ? tracker.type : "dom_event";

      // Validate handler
      if(!ns[tracker.handler]) {
        console.log("Missing KPIFF handler:", tracker.handler);
        return;
      }

      // Perform DOM query
      if (tracker.selectors) {
        var elements = context.querySelectorAll(tracker.selectors.join(","));
        for (var i = 0; i < elements.length; i++) {
          switch(tracker_type) {
            case "dom_event":
              elements[i].addEventListener('click', function(event) {
                ns[tracker.handler](event,tracker);
              });
              break;
            case "custom":
              ns[tracker.handler](elements[i],tracker);
              break;
          }
        }
      } else {
        // Handle selector-less trackers
        ns[tracker.handler](tracker);
      }

    });
  }

  /*
  * KPIFF Handlers goes here
  *
  */
  ns.handle_info_linger_once = false;
  ns.handle_info_linger = function(element, tracker) {

    // Handle linger only ONCE on info pages
    if (ns.handle_info_linger_once) {
      return;
    }

    // Only sag, tilbud, info can linger
    var linger_types = [ns.journey_type.info,ns.journey_type.sag,ns.journey_type.tilbud];
    if (! linger_types.includes(ns.getJourney())) {
      return;
    }


    ns.handle_info_linger_once = true;
    setTimeout(function() {
      ns.track(ns.journey_type.info,"timer");
    },ns.lingerTimeRequired);
  }

  ns.handle_frontdesk_init = function(element, tracker) {
    var location = new URL(document.location.href);
    var url_param = location.searchParams.get("url");
    var owner = new URL (url_param);
    var target = new URL(element.href);

    element.href =
      "HTTPS://" +
      location.hostname +
      "/?url=HTTPS://" +
      owner.hostname +
      target.pathname +
      target.search;

  }

  ns.handle_frontdesk_click = function(event, tracker) {
    console.log(event);

    var action = ns.trim(document.querySelector(".section h3").textContent);
    var label = ns.trim(event.target.textContent);

    ns.track(ns.journey_type.info, action, label, { simple_label: true} );

  }



  ns.handle_default_contact = function(event, tracker) {
    var label = [];

    // Element text
    var text = ns.truncate(event.target.textContent);
    label.push("Text:" + text);

    // Element href
    if (event.target.href) {
      const url = new URL(event.target.href);
      label.push("Host:" + url.hostname);
    }

    ns.track(ns.getJourney(),"contact", label);
  }

  ns.handle_maps = function(event, tracker) {
    ns.track(ns.journey_type.info,"map");
  }

  ns.handle_file_link = function(event, tracker) {
    ns.track(ns.journey_type.info,"file");
  }

  ns.handle_info_click = function(event, tracker) {
    ns.track(ns.journey_type.info,"click");
  }

  ns.handle_external_link = function(event, tracker) {
    var label = [];
    var journey = this.getJourney();

    // Exceptions to journey
    if (journey == ns.journey_type.ingen) {
      if (tracker.force_journey) {
        journey = tracker.force_journey;
      }
    }

    var atag;
    if (event.target.href) {
      atag = event.target;
    } else {
      atag = event.target.closest('a')
    }

    // Handle hostname of href
    if (atag.href) {
      const url = new URL(atag.href);
      label.push("Host:" + url.hostname);

      // Exceptions forcing an info journey
      const info_exceptions = [
        "facebook.com",
        "bibliotek.kk.dk",
        "brugbyen.nu",
      ]

      info_exceptions.forEach(function(host){
        if (url.hostname.indexOf(host) >= 0) {
          journey = ns.journey_type.info;
        }
      });
    }


    ns.track(journey,"External link", label);
  }

  ns.track = function(category, action, label, options) {

    var original_label = label;
    options = options || {};

    // Force KPIFF prefix
    category = "KPIFF " + category;

    // Convert label into array
    if (label == undefined) {
      label = [];
    } else if (typeof label === "string") {
      label = [ ("Value:" + label) ];
    }

    // Extend label with extra data
    var department = document.querySelector('meta[name="Department"]')
    label.push("Department:" + (department ? department.content : "None"));
    label = label.join(" ");
    // label.push("Journey:" + this.getJourney());

    if (options.simple_label) {
      label = original_label;
    }

    // Debug output
    console.log("KPIFF Category:", category);
    console.log("KPIFF Action:", action);
    console.log("KPIFF Label:", label);


    // Send event to Siteimprove
    if (typeof(_sz) !== 'undefined') {
      _sz.push(['event', category, action, label]);
    }
  }

  ns.getJourney = function() {
    var groups = document.querySelector('meta[name="Group"]');
    if (!groups) {
      // Hardcode Event pages as always Brugerrejse Tilbud
      if (document.body.classList.contains("page-node-type-event")) {
        return ns.journey_type.tilbud;
      }

      return ns.journey_type.ingen;
    }

    groups = groups.content;

    // Hack. Remove when pages have been tagged tilbud instead of event
    if (groups.indexOf("Brugerrejse Event") >= 0 ) {
      return ns.journey_type.tilbud;
    }

    // Scan KPIFF journeys
    for (const key in ns.journey_type) {
      if (groups.indexOf(ns.journey_type[key]) >= 0 ) {
        return ns.journey_type[key];
      }
    }
    return ns.journey_type.ingen;
  }

  ns.truncate = function(input) {
    input = ns.trim(input);
    if (input.length > 20) {
       return input.substring(0, 20) + '...';
    }
    return input;
  }

  // Trim whitespate from outside and inside a string.
  ns.trim = function(input) {
    return input.replace(/[\n\r]+|[\s]{2,}/g, ' ').trim()
  }

}(kpiff)



// Init the KPIFF framework
kpiff.init();

// Add eventlisteners via Drupal.behaviors or raw DOM document
document.addEventListener('DOMContentLoaded', function() {
  if (window.Drupal && Drupal.behaviors) {
    Drupal.behaviors.kpiff = {
      attach: function (context, settings) {
        kpiff.add_listeners(context,settings);
      }
    };
  } else {
    kpiff.add_listeners(document,null);
  }
}, false);

