<?php

/**
 * @file
 * Helpers for the kpiff stats page.
 */

/**
 * Preprocess the stats twig.
 */
function kpiff_preprocess_kpiff_stats(&$variables) {
  $variables += [
    'kpiff_content_count' => get_kpiff_content_count(),
    'kpiff_tagged_content_count' => get_kpiff_tagged_content_count(),
  ];
}

/**
 * Count content grouped by node type.
 */
function get_kpiff_content_count() {
  $db = \Drupal::database();

  $query = $db
    ->select('node', 'n')
    ->fields('n', ['type'])
    ->groupBy('n.type')
    ->orderBy('n.type');
  $query->addExpression('COUNT(n.type)', 'count');
  $results = $query->execute()->fetchAllKeyed();
  return $results;
}

/**
 * Fetch raw table name from field.
 */
function kpiff_get_field_table_name($entity_type_id, $field_name) {
  return \Drupal::entityTypeManager()->getStorage($entity_type_id)->getTableMapping()->getFieldTableName($field_name);
}

/**
 * Fetch property columm name from field.
 */
function kpiff_get_field_property($entity_type_id, $field_name, $property = NULL) {
  if ($property === NULL) {
    $property = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id)[$field_name]->getMainPropertyName();
  }

  $column_names = \Drupal::entityTypeManager()->getStorage($entity_type_id)->getTableMapping()->getColumnNames($field_name);

  return $column_names[$property];
}

/**
 * Traverse all taxonomy term reference fields and count content.
 */
function get_kpiff_tagged_content_count() {
  $result = [];
  $fields = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node');
  foreach ($fields as $field => $basefielddef) {
    if ($basefielddef->getType() != "entity_reference") {
      continue;
    }
    $settings = $basefielddef->getSettings();
    $target_type = $settings['target_type'] ?? "none";
    if ($target_type != "taxonomy_term") {
      continue;
    }

    $table_name = kpiff_get_field_table_name("node", $basefielddef->getName());
    $table_prop = kpiff_get_field_property("node", $basefielddef->getName());

    $db = \Drupal::database();

    $query = $db->select($table_name, 't');
    $query->addJoin('left', 'taxonomy_term_field_data', 'd', "d.tid = t." . $table_prop);
    $query->fields('d', ['name']);
    $query->orderBy('d.name');
    $query->groupBy('d.name');
    $query->addExpression('COUNT(d.name)', 'count');
    $results = $query->execute()->fetchAll();

    $result[$basefielddef->getName()] = $results;
  }
  return $result;
}
